﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class GameManager : MonoBehaviour {

	public static GameManager Instance{ get;protected set;}

	public static int CurrentUnitId;

	public ViewUnit CurrentUnit;

	public static bool IsServer = false;

	public List<EventReceiverBase> EventReceivers = new List<EventReceiverBase>();

	public List<ViewUnit> Units = new List<ViewUnit>();
	protected Dictionary<int, ViewUnit> units;

	protected NetworkConnection _connection;

	void Awake()
	{
		Instance = this;
		units = new Dictionary<int, ViewUnit>();
	}

	public virtual bool Contains(int id)
	{
		return units.ContainsKey(id);
	}
	public virtual void Set(int id, ViewUnit unit)
	{
		if(!units.ContainsKey(id))
		{
			units.Add(id, unit);
			Units.Add(unit);
		}
	}

	public virtual ViewUnit Get(int id)
	{
		ViewUnit unit;
		units.TryGetValue(id, out unit);
		return unit;
	}

	public virtual void Remove(int id)
	{
		ViewUnit unit;
		if(units.TryGetValue(id, out unit))
		{
			units.Remove(id);
			Units.Remove(unit);
		}
	}

	public virtual void Clear()
	{
		foreach(ViewUnit unit in units.Values)
		{
			Destroy(unit.gameObject);
		}
		Units.Clear();
		units.Clear();
	}

	public void Init(NetworkConnection conn)
	{
		_connection = conn;

		foreach(EventReceiverBase reciver in EventReceivers)
		{
			_connection.RegisterHandler(reciver.MsgType, reciver.ReceiverBase);
		}
	}

	public void SendAction(short actionKey, Action action)
	{
		_connection.Send(actionKey, action);
	}

}
