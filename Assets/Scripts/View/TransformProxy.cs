﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformProxy : MonoBehaviour {

	public float syncTime;
	public float syncDelay;
	public float lastSynchronizationTime;
	public Vector3 realPosition;

	public void Init(Vector3 startPosition)
	{
		realPosition = startPosition;
	}

	void Update () 
	{
		syncTime += Time.deltaTime;
        transform.position = Vector3.Lerp (transform.position, realPosition, syncTime/syncDelay);
	}

	public void Receive(Vector3 position)
	{
		syncTime = 0f;
        syncDelay = Time.time - lastSynchronizationTime;
        lastSynchronizationTime = Time.time;
		realPosition = position;
	}

	public void SetPosition(Vector3 position)
	{
		transform.position = position;
		Receive(position);
	}
}
