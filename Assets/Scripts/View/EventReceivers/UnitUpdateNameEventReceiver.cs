﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitUpdateNameEventReceiver : EventReceiver<UnitUpdateNameEvent> {

	 public override short MsgType
    {
        get
        {
            return UnitUpdateNameEvent.MsgType;
        }
    }

    protected override void Receiver(UnitUpdateNameEvent @event)
    {
        ViewUnit unit = GameManager.Instance.Get(@event.UnitId);
        if(unit != null)
        {
            unit.CurrentUnit.Name = @event.NewName;
        }
    }
}
