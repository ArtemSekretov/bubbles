﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public abstract class EventReceiverBase : MonoBehaviour
{
    public abstract short MsgType { get; }

    public abstract void ReceiverBase(NetworkMessage message);
}

public abstract class EventReceiver<T> : EventReceiverBase where T:Event, new()
{
    public override void ReceiverBase(NetworkMessage message)
    {
        Receiver(message.ReadMessage<T>());
    }

    protected abstract void Receiver(T @event);
}
