﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitGameOverEventReceiver : EventReceiver<UnitGameOverEvent>
{
    public override short MsgType
	{
		get
		{
			return UnitGameOverEvent.MsgType;
		}
	}

    protected override void Receiver(UnitGameOverEvent @event)
    {
		ScoreManager.Instance.ShowWinners(@event.WinnersId);
    }
}
