﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleControll : MonoBehaviour 
{	
	public ViewBubble Bubble;
	public bool move = false;
	public Vector3 StartPosition;
	protected Plane[] frustum;
	public Bounds bounds;

	public void Init(ViewBubble bubble)
	{
		Bubble = bubble;
		move = true;
		StartPosition = transform.position;
		frustum = GeometryUtility.CalculateFrustumPlanes(Camera.current);
		bounds = gameObject.GetComponent<MeshRenderer>().bounds;

	}

	public void Pause()
	{
		StartCoroutine(PauseC());
	}

	protected IEnumerator PauseC()
	{
		move = false;
		yield return new WaitForEndOfFrame();
		move = true;
	} 

	void Update()
	{
		if(move)
		{
			bounds.center = transform.position;
			if(!GeometryUtility.TestPlanesAABB(frustum, bounds))
			{
				transform.position = StartPosition;
				GameManager.Instance.SendAction(BubbleMoveAction.MsgType, new BubbleMoveAction(Bubble.CurrentBubble.Id, transform.position, EType.SetPosition));
				Pause();
			}
			else
			{
				transform.Translate(Bubble.CurrentBubble.Direction * Bubble.CurrentBubble.Speed * Time.deltaTime);

				GameManager.Instance.SendAction(BubbleMoveAction.MsgType, new BubbleMoveAction(Bubble.CurrentBubble.Id, transform.position, EType.Initerpolate));

			}
		}
	}
}
