﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class UnitSetNameReceiver : ActionReceiver<UnitSetNameAction>
{
    public UnitSetNameReceiver(Manager manager) : base(manager)
    {
    }

    public override short MsgType{
		get
		{
			return UnitSetNameAction.MsgType;
		}
	}

    protected override void Receiver(UnitSetNameAction action, int actorId)
    {
		Actor actor = Manager.Get(actorId);
		if(actor != null)
		{
			actor.CurrentUnit.Name = action.NewName;

			foreach(Actor a in Manager.Actors)
			{
				NetworkServer.SendToClient(a.connectionId, UnitUpdateNameEvent.MsgType, new UnitUpdateNameEvent(actor.CurrentUnit.Id, action.NewName));
			}

		}
    }
}
