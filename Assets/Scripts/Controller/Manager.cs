﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Manager : NetworkManager
{
	public List<Actor> Actors
	{
		get
		{
			return new List<Actor>(actors.Values);
		}
	}

	public string UnitName;
	public int NumberOfBubble;	
	string numberOfBubble;
	protected Dictionary<int, Actor> actors = new Dictionary<int, Actor>();

	protected GameEngine GameEngine;

	public bool isConnected = false;
	void OnGUI()
	{
		if(!isConnected)
		{
			GUILayout.BeginArea(new Rect(15, 10, 200, 50));
			GUILayout.BeginHorizontal();
			GUILayout.Label("Set Name");
			UnitName = GUILayout.TextField(UnitName);
			GUILayout.Label("Set Number Of Bubble");
			numberOfBubble = GUILayout.TextField(NumberOfBubble.ToString());
			int.TryParse(numberOfBubble, out NumberOfBubble);
			if(NumberOfBubble < 0)
			{
				NumberOfBubble = 0;
			}
			GUILayout.EndHorizontal();
			GUILayout.EndArea();
		}
	}

	public override void OnStartServer()
	{
		base.OnStartServer();
		Debug.Log("OnStartServer");
		isConnected = true;
		GameManager.IsServer = true;
		BubbleManager.Instance.Clear();
		GameManager.Instance.Clear();
		InitReceivers();
		GameEngine = new GameEngine(NumberOfBubble);
	}

	public override void OnStopServer()
	{
		base.OnStartServer();
		isConnected = false;
		Debug.Log("OnStopServer");
		GameManager.IsServer = false;
		BubbleManager.Instance.Clear();
		GameManager.Instance.Clear();
		GameEngine.Clear();

		foreach(Actor actor in actors.Values)
		{
			actor.OnRemove();
		}

		actors.Clear();
	}
	//Called on the server when a new client connects.
    public override void OnServerConnect(NetworkConnection conn)
    {
		base.OnServerConnect(conn);
        Debug.Log("OnPlayerConnected");
		int connId = conn.connectionId;
		if(!actors.ContainsKey(connId))
		{
			Actor actor = new Actor(connId);
			actor.GameEngine = GameEngine;
			actors.Add(connId, actor);
		}
    }
	public override void OnServerDisconnect(NetworkConnection conn)
	{
		base.OnServerDisconnect(conn);
		Debug.Log("OnServerDisconnect");
		int connId = conn.connectionId;
		if(actors.ContainsKey(connId))
		{
			Actor actor = actors[connId];
			actor.OnRemove();

			actors.Remove(connId);
		}

	}
    //Called on the client when connected to a server.
	public override void OnClientConnect(NetworkConnection conn)
	{
		base.OnClientConnect(conn);
		isConnected = true;
        Debug.Log("OnClientConnect");
		BubbleManager.Instance.Clear();
		GameManager.Instance.Clear();
		GameManager.Instance.Init(conn);
		GameManager.Instance.SendAction(UnitSetNameAction.MsgType, new UnitSetNameAction(UnitName));
	}
	public override void OnClientDisconnect(NetworkConnection conn)
	{
		base.OnClientDisconnect(conn);
		isConnected = false;
		BubbleManager.Instance.Clear();
		GameManager.Instance.Clear();
		Debug.Log("OnClientDisconnect");
	}

	public override void OnServerReady(NetworkConnection conn)
	{
		base.OnServerReady(conn);
		Debug.Log("OnServerReady");
		Actor actor = null;

		if(actors.TryGetValue(conn.connectionId, out actor))
		{
			actor.OnReady();

			foreach(Actor act in actors.Values)
			{
				if(act.connectionId != actor.connectionId)
				{
					NetworkServer.SendToClient(actor.connectionId, CreateUnitEvent.MsgType, new CreateUnitEvent(act.CurrentUnit));
				}
			}
		}
	}

	public virtual Actor Get(int id)
	{
		Actor actor = null;
		actors.TryGetValue(id, out actor);
		return actor;
	}

	protected virtual void InitReceivers()
	{
		List<ActionReceiverBase> actionReceivers = new List<ActionReceiverBase>();
		actionReceivers.Add(new BubbleMoveReceiver(this));
		actionReceivers.Add(new BubbleHitReceiver(this));
		actionReceivers.Add(new UnitSetNameReceiver(this));

		foreach(ActionReceiverBase receiver in actionReceivers)
		{
			NetworkServer.RegisterHandler(receiver.MsgType, receiver.ReceiverBase);
		}
	}
}