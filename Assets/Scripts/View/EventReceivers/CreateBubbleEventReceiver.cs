﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateBubbleEventReceiver : EventReceiver<CreateBubbleEvent>
{
	public ViewBubble Bubble;
    public override short MsgType
	{
		get
		{
			return CreateBubbleEvent.MsgType;
		}
	}

    protected override void Receiver(CreateBubbleEvent @event)
    {
		Bubble bub = @event.Bubble;
		ViewBubble bubble = Instantiate(Bubble, bub.Position, Quaternion.identity) as ViewBubble;
		bubble.transform.localScale = (Vector3.one * bub.Size);
		bubble.CurrentBubble = bub;
		BubbleManager.Instance.Set(bub.Id, bubble);

		if(GameManager.IsServer)
		{
			bubble.gameObject.AddComponent<BubbleControll>().Init(bubble);
		}
		else
		{
			bubble.gameObject.AddComponent<TransformProxy>().Init(bub.Position);
		}
    }
}
