﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class UnitGameOverEvent : Event
{
	public const short MsgType = 220;
	public List<int> WinnersId { get; protected set; }

	public UnitGameOverEvent()
	{

	}

	public UnitGameOverEvent(List<int> winnersId)
	{
		WinnersId = winnersId;
	}

	public override void Deserialize(NetworkReader reader)
	{
		base.Deserialize(reader);
		int count = reader.ReadInt32();
		WinnersId = new List<int>(count + 1);

		for(int i =0; i < count; i++)
		{
			WinnersId.Add(reader.ReadInt32());
		}
	}
	public override void Serialize(NetworkWriter writer)
	{
		base.Serialize(writer);
		writer.Write(WinnersId.Count);

		foreach(int id in WinnersId)
		{
			writer.Write(id);
		}
	}

}
