﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class DestroyBubbleEvent : Event 
{
	public const short MsgType = 170;
	public int BubbleId { get; protected set;}

	public DestroyBubbleEvent()
	{

	}

	public DestroyBubbleEvent(int bubbleId)
	{
		BubbleId = bubbleId;
	}
	public override void Deserialize(NetworkReader reader)
	{
		base.Deserialize(reader);
		BubbleId = reader.ReadInt32();
		
	}
	public override void Serialize(NetworkWriter writer)
	{
		base.Serialize(writer);
		writer.Write(BubbleId);
	}

}
