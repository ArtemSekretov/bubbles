﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class CreateBubbleEvent : Event
{
	public const short MsgType = 120;
	public Bubble Bubble {get; protected set;}

	public CreateBubbleEvent()
	{

	}

	public CreateBubbleEvent(Bubble bubble)
	{
		Bubble = bubble;
	}


	public override void Deserialize(NetworkReader reader)
	{
		base.Deserialize(reader);
		
		Bubble = new Bubble(reader.ReadInt32());
		Bubble.Position = reader.ReadVector3();
		Bubble.Direction = reader.ReadVector3();
		Bubble.Speed = reader.ReadSingle();
		Bubble.Size = reader.ReadSingle();
		Bubble.Health = reader.ReadInt32();
	}
	public override void Serialize(NetworkWriter writer)
	{
		base.Serialize(writer);
		writer.Write(Bubble.Id);
		writer.Write(Bubble.Position);
		writer.Write(Bubble.Direction);
		writer.Write(Bubble.Speed);
		writer.Write(Bubble.Size);
		writer.Write(Bubble.Health);
	}

}
