﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;
public class GameEngine
 {

	public List<Bubble> Bubbles{
		get
		{
			return new List<Bubble>(bubbles.Values);
		}
	}

	public int NumberOfBubble { get; protected set;}
	protected Dictionary<int, Unit> units;

	protected Dictionary<int, Bubble> bubbles;

	public GameEngine(int numberOfBubble)
	{
		units = new Dictionary<int, Unit>();

		NumberOfBubble = numberOfBubble;

		bubbles = new Dictionary<int, Bubble>(numberOfBubble);

		GenerateBubbles();
	}

	public virtual void SetUnit(Unit unit)
	{
		if(units.ContainsKey(unit.Id))
		{
			Unit u = units[unit.Id];
			u.OnExitGame();
			u = unit;
		}
		else
		{
			units.Add(unit.Id, unit);
		}
		unit.OnEnterGame();
	}

	public virtual Unit GetUnit(int id)
	{
		Unit unit;
		units.TryGetValue(id, out unit);
		return unit;
	}

	public virtual void RemoveUnit(int id)
	{
		Unit unit;
		if(units.TryGetValue(id, out unit))
		{
			unit.OnExitGame();
			units.Remove(id);
		}
	}

	public virtual void RemoveAll()
	{
		foreach(Unit unit in units.Values)
		{
			unit.OnExitGame();
		}

		units.Clear();
	}

	public virtual void Clear()
	{
		bubbles.Clear();
		RemoveAll();
	}

	public virtual Bubble GetBubble(int id)
	{
		Bubble bubble = null;
		bubbles.TryGetValue(id, out bubble);
		return bubble;
	}

	public virtual bool RemoveBubble(int id)
	{
		return bubbles.Remove(id);
	}

	public virtual void HitBubble(Unit unit, int bubbleid)
	{
		Bubble bubble = GetBubble(bubbleid);
		if(bubble != null)
		{
			bubble.Health--;
			if(bubble.Health <= 0)
			{
				unit.Score ++;
				unit.SendEvent(UnitUpdateScoreEvent.MsgType, new UnitUpdateScoreEvent(unit.Id, unit.Score));
				RemoveBubble(bubbleid);
				unit.SendEvent(DestroyBubbleEvent.MsgType, new DestroyBubbleEvent(bubbleid));

				if(bubbles.Count <= 0)
				{
					int maxScore = units.Max(x=> x.Value.Score);

					List<int> winners = new List<int>();

					foreach(Unit u in units.Values)
					{
						if(u.Score == maxScore)
						{
							winners.Add(u.Id);
						}
					}

					unit.SendEvent(UnitGameOverEvent.MsgType, new UnitGameOverEvent(winners));
				}
			}
			else
			{
				//send event update bubble health
			}
		}
	}

	protected virtual void GenerateBubbles()
	{
		for(int i= 0 ; i < NumberOfBubble; i++)
		{
			Bubble bubble = CreateBubble();
			bubbles.Add(bubble.Id, bubble);
		}
	}

	protected virtual Bubble CreateBubble()
	{
		Bubble bubble = new Bubble(UnityEngine.Random.Range(1, int.MaxValue - 1));
		bubble.Position = new Vector3(UnityEngine.Random.Range(-2.0f, 2.0f), UnityEngine.Random.Range(-2.0f, 2.0f));
		bubble.Direction = new Vector3(UnityEngine.Random.Range(-1.0f, 1.0f), UnityEngine.Random.Range(-1.0f, 1.0f));
		bubble.Speed = UnityEngine.Random.Range(1.0f, 3.0f);
		bubble.Size = UnityEngine.Random.Range(0.5f, 2.0f);
		bubble.Health = UnityEngine.Random.Range(1, 3);
		return bubble;
	}
}
