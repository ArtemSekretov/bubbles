﻿public class BubbleMoveEventReceiver : EventReceiver<BubbleMoveEvent>
{
    public override short MsgType
	{
		get
		{
			return BubbleMoveEvent.MsgType;
		}
	}

    protected override void Receiver(BubbleMoveEvent @event)
    {
		ViewBubble bubble = BubbleManager.Instance.Get(@event.BubbleId);
		if(bubble != null)
		{
			TransformProxy transformProxy = bubble.GetComponent<TransformProxy>();
			if(transformProxy != null)
			{
				bubble.CurrentBubble.Position = @event.BubblePosition;
				switch(@event.MoveType)
				{
					case EType.Initerpolate:
						transformProxy.Receive(@event.BubblePosition);
						break;
					case EType.SetPosition:
						transformProxy.SetPosition(@event.BubblePosition);
						break;
				}
			}
		}
    }
}
