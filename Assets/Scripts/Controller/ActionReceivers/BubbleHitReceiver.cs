﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleHitReceiver : ActionReceiver<BubbleHitAction>
{
    public BubbleHitReceiver(Manager manager) : base(manager)
    {
    }

    public override short MsgType
	{
		get
		{
			return BubbleHitAction.MsgType;
		}
	}

    protected override void Receiver(BubbleHitAction action, int actorId)
    {
		Actor actor = Manager.Get(actorId);
		if(actor != null)
		{
			actor.GameEngine.HitBubble(actor.CurrentUnit, action.BubbleId);
		}
    }
}
