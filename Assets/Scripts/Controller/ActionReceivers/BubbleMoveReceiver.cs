﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class BubbleMoveReceiver : ActionReceiver<BubbleMoveAction>
 {

    public override short MsgType
	{
		get
		{
			return BubbleMoveAction.MsgType;
		}
	}
    public BubbleMoveReceiver(Manager manager) : base(manager)
    {
    }

    protected override void Receiver(BubbleMoveAction action, int actorId)
    {
        Actor actor = Manager.Get(actorId);

		Bubble bubble = actor.GameEngine.GetBubble(action.BubbleId);
		if(bubble != null)
		{
			bubble.Position = action.BubblePosition;

			foreach(Actor a in Manager.Actors)
			{
				if(a.connectionId != actorId)
				{
					NetworkServer.SendToClient(a.connectionId, BubbleMoveEvent.MsgType, new BubbleMoveEvent(action.BubbleId, action.BubblePosition, action.MoveType));
				}
			}
		}
    }
}
