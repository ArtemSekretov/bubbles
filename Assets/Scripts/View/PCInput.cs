﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PCInput : MonoBehaviour {

	public LayerMask hitMask;
	public Camera MyCamera;
	
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetMouseButtonUp(0))
		{
			RaycastHit hit;
			Ray ray = MyCamera.ScreenPointToRay(Input.mousePosition);
        
			if (Physics.Raycast(ray, out hit, 10000.0f, hitMask)) 
			{
				Transform objectHit = hit.transform;
				ViewBubble bubble = objectHit.GetComponent<ViewBubble>();
				if(bubble != null)
				{
					GameManager.Instance.SendAction(BubbleHitAction.MsgType, new BubbleHitAction(bubble.CurrentBubble.Id));
				}
			}
		}
	}
}
