﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class BubbleHitAction : Action
{
	public const short MsgType = 160;
	public int BubbleId { get; protected set;}

	public BubbleHitAction()
	{

	}

	public BubbleHitAction(int bubbleId)
	{
		BubbleId = bubbleId;
	}
	public override void Deserialize(NetworkReader reader)
	{
		base.Deserialize(reader);
		BubbleId = reader.ReadInt32();
		
	}
	public override void Serialize(NetworkWriter writer)
	{
		base.Serialize(writer);
		writer.Write(BubbleId);
	}

}
