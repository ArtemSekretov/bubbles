﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyBubbleEventReceiver : EventReceiver<DestroyBubbleEvent>
{
    public override short MsgType
	{
		get
		{
			return DestroyBubbleEvent.MsgType;
		}
	}

    protected override void Receiver(DestroyBubbleEvent @event)
    {
        ViewBubble bubble = BubbleManager.Instance.Get(@event.BubbleId);
		if(bubble != null)
		{
			BubbleManager.Instance.Remove(@event.BubbleId);
			Destroy(bubble.gameObject);
		}
    }
}
