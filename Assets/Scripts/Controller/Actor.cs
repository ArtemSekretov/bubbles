﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Actor
{
	public int connectionId { get; protected set;}
	public GameEngine GameEngine {get; set;}

	public Unit CurrentUnit { get; protected set;}

	public Actor(int connId)
	{
		connectionId = connId;

		CurrentUnit = new Unit(Random.Range(0, int.MaxValue - 1));
	}

	public virtual void OnReady()
	{
		SendPrivateEvent(CurrentUnitIdEvent.MsgType, new CurrentUnitIdEvent(CurrentUnit.Id));
		CurrentUnit.AddSendEventHandler(SendEvent);
		CurrentUnit.AddSendPrivateEventHandler(SendPrivateEvent);

		GameEngine.SetUnit(CurrentUnit);

		foreach(Bubble bubble in GameEngine.Bubbles)
		{
			SendPrivateEvent(CreateBubbleEvent.MsgType, new CreateBubbleEvent(bubble));
		}


	}

	public virtual void OnRemove()
	{
		CurrentUnit.RemoveSendEventHandler(SendEvent);
		CurrentUnit.RemoveSendPrivateEventHandler(SendPrivateEvent);
		GameEngine.RemoveUnit(CurrentUnit.Id);
	}

	protected virtual void SendEvent(short eventKey, Event @event)
	{
		NetworkServer.SendToAll(eventKey, @event);
	}

	protected virtual void SendPrivateEvent(short eventKey, Event @event)
	{
		NetworkServer.SendToClient(connectionId, eventKey, @event);
	}

}
