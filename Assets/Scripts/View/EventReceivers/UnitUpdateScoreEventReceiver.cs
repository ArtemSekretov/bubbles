﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitUpdateScoreEventReceiver : EventReceiver<UnitUpdateScoreEvent>
{
    public override short MsgType
    {
        get
        {
            return UnitUpdateScoreEvent.MsgType;
        }
    }

    protected override void Receiver(UnitUpdateScoreEvent @event)
    {
        ViewUnit unit = GameManager.Instance.Get(@event.UnitId);
        if(unit != null)
        {
            unit.CurrentUnit.Score = @event.NewScore;
        }
    }
}
