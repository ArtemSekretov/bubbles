﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bubble
{
	public int Id{ get; protected set;}

	public float Speed{ get; set;}

	public Vector3 Direction { get; set;}

	public Vector3 Position { get; set; }

	public float Size { get; set;}

	public int Health { get; set; }

	public Bubble (int id)
	{
		Id = id;
	}
}
