﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class UnitUpdateScoreEvent : Event
{
	public const short MsgType = 180;
	public int UnitId { get; protected set;}
	public int NewScore { get; protected set;}
	public UnitUpdateScoreEvent()
	{

	}

	public UnitUpdateScoreEvent(int unitId, int newScore)
	{
		UnitId = unitId;
		NewScore = newScore;
	}
	public override void Deserialize(NetworkReader reader)
	{
		base.Deserialize(reader);
		UnitId = reader.ReadInt32();
		NewScore = reader.ReadInt32();
		
	}
	public override void Serialize(NetworkWriter writer)
	{
		base.Serialize(writer);
		writer.Write(UnitId);
		writer.Write(NewScore);
	}


}
