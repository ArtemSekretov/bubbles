﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class BubbleMoveAction : Action
{
	public const short MsgType = 130;

	public int BubbleId { get; protected set;}

	public Vector3 BubblePosition { get; protected set;}

	public EType MoveType { get; protected set;}	
	public BubbleMoveAction()
	{

	}

	public BubbleMoveAction(int id, Vector3 position, EType moveType)
	{
		BubbleId = id;
		BubblePosition = position;
		MoveType = moveType;
	}

	public override void Deserialize(NetworkReader reader)
	{
		base.Deserialize(reader);

		BubbleId = reader.ReadInt32();
		BubblePosition = reader.ReadVector3();
		MoveType = (EType)reader.ReadByte();
		
	}
	public override void Serialize(NetworkWriter writer)
	{
		base.Serialize(writer);

		writer.Write(BubbleId);
		writer.Write(BubblePosition);
		writer.Write((byte)MoveType);
	}

}
