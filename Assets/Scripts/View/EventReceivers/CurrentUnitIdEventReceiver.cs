﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrentUnitIdEventReceiver : EventReceiver<CurrentUnitIdEvent>
{
    public override short MsgType
	{
		get
		{
			return CurrentUnitIdEvent.MsgType;
		}
	}

    protected override void Receiver(CurrentUnitIdEvent @event)
    {
		GameManager.CurrentUnitId = @event.CurrentUnitId;
    }
}
