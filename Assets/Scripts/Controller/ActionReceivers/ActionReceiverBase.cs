using System;
using UnityEngine;
using UnityEngine.Networking;

public abstract class ActionReceiverBase
{
    public abstract short MsgType { get; }

    protected Manager Manager;
    protected ActionReceiverBase(Manager manager)
    {
        Manager = manager;
    }

    public abstract void ReceiverBase(NetworkMessage message);
}

public abstract class ActionReceiver<T> : ActionReceiverBase where T:Action, new()
{
    public ActionReceiver(Manager manager) : base(manager)
    {
    }

    public override void ReceiverBase(NetworkMessage message)
    {
        Receiver(message.ReadMessage<T>(), message.conn.connectionId);
    }

    protected abstract void Receiver(T action, int actorId);
}
