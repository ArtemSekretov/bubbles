﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateUnitEventReceiver : EventReceiver<CreateUnitEvent>
{
	public ViewUnit Unit;
    public override short MsgType
	{
		get
		{
			return CreateUnitEvent.MsgType;
		}
	}

    protected override void Receiver(CreateUnitEvent @event)
    {
		Unit u = @event.CurrentUnit;
		if(!GameManager.Instance.Contains(u.Id))
		{
			ViewUnit unit = Instantiate(Unit, Vector3.zero, Quaternion.identity) as ViewUnit;
			unit.name = u.Name + " " + u.Id;
			unit.CurrentUnit = u;
			if(GameManager.CurrentUnitId == u.Id)
			{
				GameManager.Instance.CurrentUnit = unit;
			}
			GameManager.Instance.Set(u.Id, unit);
		}
		else
		{
			Debug.LogError("Unit All ready Exist");
		}
    }
}
