﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class UnitUpdateNameEvent : Event {

	public const short MsgType = 210;
	public int UnitId { get; protected set;}
	public string NewName {get; protected set;}

	public UnitUpdateNameEvent()
	{

	}

	public UnitUpdateNameEvent(int unitId, string newName)
	{
		NewName = newName;
		UnitId = unitId;
	}

	public override void Deserialize(NetworkReader reader)
	{
		base.Deserialize(reader);
		UnitId = reader.ReadInt32();
		NewName = reader.ReadString();
	}
	public override void Serialize(NetworkWriter writer)
	{
		base.Serialize(writer);
		writer.Write(UnitId);
		writer.Write(NewName);
	}
}
