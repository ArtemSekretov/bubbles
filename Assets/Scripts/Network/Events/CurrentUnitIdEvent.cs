﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class CurrentUnitIdEvent : Event 
{	
	public const short MsgType = 110;
	public int CurrentUnitId { get; protected set;}

    public CurrentUnitIdEvent()
	{
		
	}
	public CurrentUnitIdEvent(int currentUnitId)
	{
		CurrentUnitId = currentUnitId;
	}

	public override void Deserialize(NetworkReader reader)
	{
		base.Deserialize(reader);
		CurrentUnitId = reader.ReadInt32();
	}
	public override void Serialize(NetworkWriter writer)
	{
		base.Serialize(writer);
		writer.Write(CurrentUnitId);
	}
}
