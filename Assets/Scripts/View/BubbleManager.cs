﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleManager : MonoBehaviour {

	public static BubbleManager Instance{ get;protected set;}

	public List<ViewBubble> Bubbles = new List<ViewBubble>();
	protected Dictionary<int, ViewBubble> bubbles;

	void Awake()
	{
		Instance = this;
		bubbles = new Dictionary<int, ViewBubble>();
	}

	public virtual bool Contains(int id)
	{
		return bubbles.ContainsKey(id);
	}
	public virtual void Set(int id, ViewBubble bubble)
	{
		if(!bubbles.ContainsKey(id))
		{
			bubbles.Add(id, bubble);
			Bubbles.Add(bubble);
		}
	}

	public virtual ViewBubble Get(int id)
	{
		ViewBubble bubble;
		bubbles.TryGetValue(id, out bubble);
		return bubble;
	}

	public virtual void Remove(int id)
	{
		ViewBubble bubble;
		if(bubbles.TryGetValue(id, out bubble))
		{
			bubbles.Remove(id);
			Bubbles.Remove(bubble);
		}

	}

	public virtual void Clear()
	{
		foreach(ViewBubble bubble in bubbles.Values)
		{
			Destroy(bubble.gameObject);
		}
		Bubbles.Clear();
		bubbles.Clear();
	}
}
