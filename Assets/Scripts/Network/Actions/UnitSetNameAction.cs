﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class UnitSetNameAction : Action
{
	public const short MsgType = 200;
	public string NewName {get; protected set;}

	public UnitSetNameAction()
	{

	}

	public UnitSetNameAction(string newName)
	{
		NewName = newName;
	}

	public override void Deserialize(NetworkReader reader)
	{
		base.Deserialize(reader);
		
		NewName = reader.ReadString();
	}
	public override void Serialize(NetworkWriter writer)
	{
		base.Serialize(writer);
		writer.Write(NewName);
	}

}
