﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour {

	public static ScoreManager Instance{ get;protected set;}
	
	List<int> Winners;

	void Awake()
	{
		Instance = this;
		Winners = new List<int>();
	}

	void OnGUI()
	{
		GUILayout.BeginArea(new Rect(Screen.width - 200, 10, 200, 200), GUI.skin.box);
		GUILayout.BeginVertical();
		foreach(ViewUnit unit in GameManager.Instance.Units)
		{
			string label = unit.CurrentUnit.Name + " Score [ " + unit.CurrentUnit.Score + " ]";
			if(Winners.Contains(unit.CurrentUnit.Id))
			{
				label += " WINNER!!!";
			}
			GUILayout.Label(label);
		}
		GUILayout.EndVertical();
		GUILayout.EndArea();
	}

	public void ShowWinners(List<int> winners)
	{
		Winners = winners;
	}
}
