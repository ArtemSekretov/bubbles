﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class CreateUnitEvent : Event {
	public const short MsgType = 150;

	public Unit CurrentUnit { get; protected set; }

	public CreateUnitEvent()
	{

	}

	public CreateUnitEvent(Unit unit)
	{
		CurrentUnit = unit;
	}

	public override void Deserialize(NetworkReader reader)
	{
		base.Deserialize(reader);
		
		CurrentUnit = new Unit(reader.ReadInt32());
		CurrentUnit.Name = reader.ReadString();
		CurrentUnit.Score = reader.ReadInt32();
	}
	public override void Serialize(NetworkWriter writer)
	{
		base.Serialize(writer);
		writer.Write(CurrentUnit.Id);
		writer.Write(CurrentUnit.Name);
		writer.Write(CurrentUnit.Score);
	}

}
