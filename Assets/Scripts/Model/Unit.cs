﻿using System.Collections;
using System.Collections.Generic;

public class Unit
{
	public event System.Action<short, Event> OnSendEvent;
	public event System.Action<short, Event> OnSendPrivateEvent;

	public int Id { get; protected set;}

	public string Name { get; set; }

	public int Score { get; set; }

	public Unit(int id)
	{
		Id = id;
		Score = 0;
		Name = "Player";
	}

	public virtual void OnEnterGame()
	{
		SendEvent(CreateUnitEvent.MsgType, new CreateUnitEvent(this));
	}

	public virtual void OnExitGame()
	{
		//remove
	}

	public virtual void AddSendEventHandler(System.Action<short, Event> handler)
	{
		OnSendEvent += handler;
	}

	public virtual void RemoveSendEventHandler(System.Action<short, Event> handler)
	{
		OnSendEvent -= handler;
	}

	public virtual void SendEvent(short eventKey, Event @event)
	{
		if(OnSendEvent != null)
		{
			OnSendEvent(eventKey, @event);
		}
	}

	public virtual void AddSendPrivateEventHandler(System.Action<short, Event> handler)
	{
		OnSendPrivateEvent += handler;
	}

	public virtual void RemoveSendPrivateEventHandler(System.Action<short, Event> handler)
	{
		OnSendPrivateEvent -= handler;
	}

	public virtual void SendPrivateEvent(short eventKey, Event @event)
	{
		if(OnSendPrivateEvent != null)
		{
			OnSendPrivateEvent(eventKey, @event);
		}
	}

}
